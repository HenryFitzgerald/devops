terraform {
  backend "s3" {
    key     = "network_tfstate"
    region  = "eu-west-1"
    encrypt = true
  }

  required_version = "~> 0.11.10"
}

resource "aws_key_pair" "ssh_key" {
  key_name   = "${local.env}-ssh-pubkey"
  public_key = "${file(local.sshpubkey_file)}"
}
