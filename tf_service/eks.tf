module "my_eks" {
  name   = "${local.env}-eks"
  source = "./modules/eks"

  vpc_id               = "${data.terraform_remote_state.network.vpc_id}"
  subnet_ids           = ["${concat(data.terraform_remote_state.network.private_subnet_ids, data.terraform_remote_state.network.public_subnet_ids)}"]
  private_subnet_ids   = ["${concat(data.terraform_remote_state.network.private_subnet_ids)}"]
  worker_instance_type = "t3.small"
  worker_desired_count = 1
  worker_max_count     = 1
  worker_min_count     = 1

  key_name = "${data.terraform_remote_state.network.ssh_key_name}"
}
